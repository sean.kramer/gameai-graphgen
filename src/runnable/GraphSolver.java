package runnable;
import graphs.Algorithm;
import graphs.Node;
import heuristics.Constant;
import heuristics.Distance;
import heuristics.Heuristic;
import heuristics.Elongate;
import heuristics.Manhattan;
import heuristics.QuasiRandom;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.KeyEvent;


@SuppressWarnings("serial")
public class GraphSolver extends PApplet {
	
	public static final String TITLE = "GraphSolver";
	public static final int TEXT_SIZE = 10;
	public static String opener = null;
	
	Algorithm alg = null;
	
	private List<Node> nodes = new ArrayList<>();
	private Node start = null;
	private Node end = null;
	private Node seek = new Node(-1, new PVector());

	private boolean drawEdges = false;
	private boolean zoom = false;
	private int heuristic = 1;
	private int zx, zy;
	private float zs = 1;
	
	private boolean checking = false;
	private boolean check_flip;
	private int checkx, checky;
	private int checked;
	private float tmpSpace;
	private float tmpPath;
	private float tmpTime;
	private float spaceEff;
	private float pathEff;
	private float timeEff;
	
	public static final Heuristic[] HEURISTICS = {
		new Constant(0),
		new Distance(),
		new Manhattan(),
		new QuasiRandom(),
		new Elongate(),
	};

	
	public GraphSolver() {
		if (opener != null)
			opener(opener);
		else {
			System.err.println("Variable 'opener' must be set to a filename before this class is instantiated.");
			exit();
		}
	}


	@Override
	public void keyPressed(KeyEvent e) {
		switch (key) {
		case TAB: drawEdges = !drawEdges; break;
		case 's': if (!checking) Algorithm.toggleStepping(); break;
		case 'q': if (checking) checking = false; alg = null; break;
		case 'r': randomize(); break;
		case 'e': check(); break;
		case ' ': step(); break;
		default:
			int i = key - '1';
			if (i >= 0 && i < HEURISTICS.length) {
				heuristic = i;
				solve();				
			}
		}
	}

	
	private void check() {
		if (checking) {
			checking = false;
			return;
		}
		if (alg == null) {
			JOptionPane.showMessageDialog(null, "You must select a heurstic before you can begin evaluating it.");
			return;
		}
		if (Algorithm.getStepping()) Algorithm.toggleStepping();
		checked = 0;
		spaceEff = 0;
		timeEff = 0;
		pathEff = 0;
		checkx = 0;
		checky = 0;
		check_flip = false;
		checking = true;
	}
	
	private float ms = 0;
	private void checkStep() {
		if (checky == nodes.size()) {
			checkx++;
			checky = 0;
		}
		if (checkx == nodes.size()) return;
		if (checkx == checky) {
			checky++;
			return;
		}
		
		start = nodes.get(checkx);
		end = nodes.get(checky);
		
		//Test heuristic
		if (check_flip) {
			alg = new Algorithm(start, end, HEURISTICS[heuristic]);
			alg.step();
			
			checked++;
			checky++;
			
			spaceEff += tmpSpace / alg.getStep();
			pathEff += tmpPath / alg.getPathLength();
			if (alg.time > 0)
				timeEff += tmpTime / alg.time;
			else
				timeEff += timeEff / checked;
			ms += alg.time;
			
		//Dijkstra's
		} else {
			alg = new Algorithm(start, end);
			alg.step();
			
			if (!alg.getSolved()) {
				checky++;
				return;
			}
			
			tmpSpace = alg.getStep();
			tmpPath = alg.getPathLength();
			tmpTime = alg.time;
		}
		
		check_flip = !check_flip;
	}
	
	
	private void step() {
		if (alg == null) {
			solve();
			return;
		}
		alg.step();
	}


	private void title() {
		String title = "";
		if (checking) {
			title += "Evaluating: " + HEURISTICS[heuristic].getName();
			title += " | Checked: " + checked;
			title += String.format(" | Space Eff: %.1f%% | Path Eff: %.1f%% | Time Eff: %.1f%% (%.1fms)",
					spaceEff / checked * 100, pathEff / checked * 100, timeEff / checked * 100, ms / checked);
		} else if (alg == null) {
			title += "Heuristics: " + HEURISTICS.length + ")";
			title += " | StepFilter: " + Algorithm.getStepping() + " (s)";
			title += " | ShowEdges: " + drawEdges + " (tab)";
		} else {
			title += alg.getHeuristicName();
			title += " | Step: " + alg.getStep();
			title += " | State: " + alg.getState();
			title += " | Path: " + alg.getPathLength();
		}
		frame.setTitle(title);
	}


	private void solve() {
		if (start == null || end == null) {
			JOptionPane.showMessageDialog(null, "First set a start and end location with left and right click.");
			return;
		}

		//Create an algorithm to solve the graph with the appropriate heuristic
		alg = new Algorithm(start, end, HEURISTICS[heuristic]);
		step();
	}



	@Override
	public void mousePressed() {
		if (mouseButton == 3 && delta < 20) {
			if (zoom) {
				zoom = false;
				textSize(TEXT_SIZE);
				zs = 1;
				return;
			}
			zs = 20f / delta;
			textSize(TEXT_SIZE / zs);

			zoom = true;
			zx = mouseX;
			zy = mouseY;
			return;
		}

		seek.p.x = mouseX;
		seek.p.y = mouseY;
		if (zoom) {
			seek.p.x = (int) ((seek.p.x - zx) / zs + zx);
			seek.p.y = (int) ((seek.p.y - zy) / zs + zy);
		}
		seek.p.x = seek.p.x - seek.p.x % delta;
		seek.p.y = seek.p.y - seek.p.y % delta;

		int i = nodes.indexOf(seek);
		if (i < 0) return;
		if (mouseButton == LEFT)
			start = nodes.get(i);
		if (mouseButton == RIGHT)
			end = nodes.get(i);

		if (start == end)
			end = null;
	}

	@Override
	public void draw() {
		resetMatrix();

		if (checking && (checkx < nodes.size())) {
			for (int i = 0; i < 200; i++) {
				 {
					checkStep();
					checkStep();
				}
			}
			if (check_flip && checkx < nodes.size())
				checkStep();
		}
		
		if (zoom) {
			translate(-zx * zs + zx, -zy * zs + zy);
			scale(zs);
		}

		title();
		background(0xffcccccc);
		if (drawEdges) {
			stroke(0x88990000);
			for (Node n : nodes)
				for (Node m : n.edges)
					if (n.i < m.i)
						line(n.p.x + hd, n.p.y + hd, m.p.x + hd, m.p.y + hd);
			noStroke();
		}

		fill(0xff000000);
		for (Node n : nodes)
			rect(n.p.x, n.p.y, delta, delta);

		if (start != null) {
			fill(0xff00ffff);
			rect(start.p.x, start.p.y, delta, delta);
		}
		if (end != null) {
			fill(0xffff9933);
			rect(end.p.x, end.p.y, delta, delta);
		}


		if (alg != null) {
			fill(0xbbff0000);
			for (Node n : alg.getClosed())
				rect(n.p.x, n.p.y, delta, delta);
			fill(0xbbaa00ff);
			for (Node n : alg.getOpen()) 
				rect(n.p.x, n.p.y, delta, delta);
			if (delta >= 20 || zoom) {
				fill(0xffffffff);
				for (Node n : alg.getOpen()) {
					text((int) n.g, n.p.x, n.p.y + hd);
					text((int) n.h, n.p.x, n.p.y + delta);
				}
				for (Node n : alg.getClosed()) {
					text((int) n.g, n.p.x, n.p.y + hd);
					text((int) n.h, n.p.x, n.p.y + delta);
				}
			}
		}

		if (alg != null && alg.getSolved()) {
			stroke(0xffffff00);
			Node[] path = alg.getPath();
			Node n, m;
			for (int i = 0; i < path.length - 1; i++) {
				n = path[i];
				m = path[i + 1];
				line(n.p.x + hd, n.p.y + hd, m.p.x + hd, m.p.y + hd);
			}
			noStroke();
		}
	}

	
	Random r = new Random();
	private void randomize() {
		start = nodes.get(r.nextInt(nodes.size()));
		do {end = nodes.get(r.nextInt(nodes.size()));}
		while (start == end);
	}
	

	int delta, hd;
	@Override
	public void setup() {
		delta = Math.min((displayWidth - 100) / (X - MX), (displayHeight - 200) / (Y - MY));
		if (delta < 2) delta = 2;
		if (delta > 40) delta = 40;
		hd = delta / 2;

		//Normalize node positions
		for (Node n : nodes) {
			n.p.x -= MX;
			n.p.y -= MY;
			n.p.x *= delta;
			n.p.y *= delta;
			n.p.x += 2 * delta;
			n.p.y += 2 * delta;
			for (int i = 0; i < n.weights.size(); i++)
				n.weights.set(i, n.weights.get(i) * delta);
		}

		size(delta * (X - MX), delta * (Y - MY));
		ellipseMode(CENTER);
		frameRate(30);
		noStroke();
		textSize(TEXT_SIZE);
		strokeWeight(Math.max(delta / 2, 1));
		smooth();
	}


	int X = Integer.MIN_VALUE, Y = Integer.MIN_VALUE;
	int MX = Integer.MAX_VALUE, MY = Integer.MAX_VALUE;
	private void opener(String file) {
		try {
			Scanner in = new Scanner(new File(file));
			int n = in.nextInt();
			for (int i = 0; i < n; i++) {
				int xx = (int) in.nextFloat();
				int yy = (int) in.nextFloat();
				if (xx > X) X = xx;
				if (yy > Y) Y = yy;
				if (xx < MX) MX = xx;
				if (yy < MY) MY = yy;
				nodes.add(new Node(i, new PVector(xx, yy)));
			}
			n = in.nextInt();
			for (int i = 0; i < n; i++) {
				Node a = nodes.get(in.nextInt());
				Node b = nodes.get(in.nextInt());
				a.doubleEdge(b, PVector.dist(a.p, b.p));
			}
			in.close();
			X += 5;
			Y += 5;
		} catch (Exception e) {}
	}


	private static String DEFAULT = null;//"test"; //Set to null to prompt for file
	public static void main(String[] args) {
		opener = GraphDraw.toFileName(DEFAULT == null ? ((String) JOptionPane.showInputDialog(null)) : DEFAULT);
		if (opener == null)
			System.err.println("No filename provided. Exiting.");
		else
			PApplet.main("runnable.GraphSolver");
	}
}