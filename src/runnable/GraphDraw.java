package runnable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;

@SuppressWarnings("serial")
public class GraphDraw extends PApplet {

	public static String toFileName(String name) {
		if (name == null) return null;
		return "data/" + name.split("\\.")[0] + ".grph";
	}
	
	public GraphDraw() {
		if (opener != null)
			openfile(opener);
	}
	
	int X = 40;
	int delta;
	int hd;
	
	int tx = 0;
	int ty = 0;
	
	PVector sel;
	int x, y;
	
	private class Edge {
		PVector a, b;
		
		public Edge(PVector a, PVector b) {
			this.a = a;
			this.b = b;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof Edge)) return false;
			Edge e = (Edge) o;
			if ((a == e.a && b == e.b) || (a == e.b && b == e.a))
				return true;
			return false;
		}
	}
	
	List<Edge> edges = new ArrayList<>();
	List<PVector> nodes = new ArrayList<>();
	
	public static final float SCALE = 0.45f;
	
	public static void main(String[] args) {
		PApplet.main("runnable.GraphDraw");
	}
	
	
	public static String opener = null;
	
	
	private void setPos(MouseEvent e) {
		x = (e.getX() - tx) / delta;
		y = (e.getY() - ty) / delta;
	}
	
	public void mouseMoved(MouseEvent e) {
		setPos(e);		
	}
	
	public void mouseDragged(MouseEvent e) {
		setPos(e);	
	}
	
	public void mousePressed(MouseEvent e) {
		setPos(e);	
		PVector loc = new PVector(x, y);
		int i = nodes.indexOf(loc);
		if (mouseButton == 39) {
			if (i >= 0) {
			loc = nodes.get(i);
			for (int m = 0; m < edges.size(); m++) {
				Edge edge = edges.get(m);
				if (edge.a == loc || edge.b == loc) {
					edges.remove(m);
					m--;
				}
			}
			nodes.remove(i);
			}
			return;
		} 
		if (i < 0)
			nodes.add(loc);
		else
			sel = nodes.get(i);
	}
	
	int speed = 3;
	@Override
	public void keyPressed() {
		if (key == 'p') {
			write(toFileName((String) JOptionPane.showInputDialog(null)));
		} if (key == 'g') {
			String s = toFileName((String) JOptionPane.showInputDialog(null));
			write(s);
			GraphSolver.opener = s;
			PApplet.main("runnable.GraphSolver");
		} else if (key == 'o') {
			openfile(toFileName((String) JOptionPane.showInputDialog(null)));
		}
		
		if (key == 'a')
			h = speed;
		if (key == 'd')
			h = -speed;
		if (key == 'w')
			v = speed;
		if (key == 's')
			v = -speed;
	}
	
	
	public void keyReleased() {
		if (key == 'a')
			h = 0;
		if (key == 'd')
			h = 0;
		if (key == 'w')
			v = 0;
		if (key == 's')
			v = 0;
	}

	
	int v = 0, h = 0;
	
	
	@Override
	public void mouseWheel(MouseEvent event) {
		scale += -event.getCount();
	}
	
	
	private void openfile(String file) {
		int max = 1;
		try {
			Scanner in = new Scanner(new File(file));
			edges.clear();
			nodes.clear();
			int n = in.nextInt();
			for (int i = 0; i < n; i++) {
				int xx = (int) in.nextFloat();
				int yy = (int) in.nextFloat();
				if (xx > max) max = xx;
				if (yy > max) max = yy;
				nodes.add(new PVector(xx, yy));
			}
			n = in.nextInt();
			for (int i = 0; i < n; i++)
				edges.add(new Edge(nodes.get(in.nextInt()), nodes.get(in.nextInt())));
			in.close();
			X = max + 10;
		} catch (Exception e) {}
	}
	
	
	private void write(String file) {
		try {
			PrintStream writer = new PrintStream(new FileOutputStream(file));
			writer.println(nodes.size());
			for (PVector n : nodes)
				writer.println((int) n.x + " " + (int) n.y);
			writer.println(edges.size());
			for (Edge e : edges)
				writer.println(nodes.indexOf(e.a) + " " + nodes.indexOf(e.b));
			writer.close();
		} catch (Exception e) {
		}
	}
	
	
	public void mouseReleased(MouseEvent e) {
		setPos(e);
		if (sel == null) return;
		
		PVector loc = new PVector(x, y);
		int i = nodes.indexOf(loc);
		if (i >= 0) {
			Edge edge = new Edge(sel, nodes.get(i));
			i = edges.indexOf(edge);
			if (i >= 0)
				edges.remove(i);
			else if (edge.a != edge.b)
				edges.add(edge);
		}
		sel = null;
	}
	
	int scale = 0;
	
	public void setup() {
		size((int) (displayWidth * SCALE), (int) (displayHeight * SCALE));
		frame.setResizable(true);
		frame.setTitle("GraphDraw");
		
		delta = displayWidth / X + scale;
		hd = delta / 2;
		
		ellipseMode(CENTER);
		
		frameRate(60);
		noStroke();
		strokeWeight(2);
		smooth();
	}
	
	
	public void draw() {
		tx += h;
		ty += v;
		g.translate(tx, ty);
		
		delta = width / X + scale;
		if (delta < 4) delta = 4;
		hd = delta / 2;
		
		background(0xffcccccc);
		fill(0xff00aaff);
		rect(x * delta, y * delta, delta, delta);
		fill(0xff000000);
		
		stroke(0xff551111);
		for (Edge e : edges)
			line(e.a.x * delta + hd, e.a.y * delta + hd, e.b.x * delta + hd, e.b.y * delta + hd);
		noStroke();
			
		for (PVector n : nodes)
			ellipse(n.x * delta + hd, n.y * delta + hd, hd, hd);
		fill(0xffff0000);
		if (sel != null)
			ellipse(sel.x * delta + hd, sel.y * delta + hd, hd, hd);
	}
	
}
