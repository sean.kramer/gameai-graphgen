package runnable;

import entities.Boid;
import graphs.Algorithm;
import graphs.Node;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Scanner;

import javax.swing.JOptionPane;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import util.Simulation;
import behaviors.AlignToVelocity;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.SeekTarget;

public class Game extends Simulation {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;

	private static final int BOUND = 20;
	private static final float HALF = BOUND / 2 + 0.001f;
	private static final int NODE_SIZE = 8;

	private Node[][] nodes;
	private Node goal;
	private PVector fineGoal;
	private Algorithm alg;
	private int xOff, yOff;
	private int heuristic = 1;
	
	private Boid boid;
//	private AvoidPositions avoid;
	private BreadCrumbs crumb;
	private SeekTarget target;

	private Grid highlight = null;

	private boolean rightDown = false;
	private boolean smoothing = true;


	public Game() {
		super();
		initGrid();
		target = new SeekTarget(200, 200, 1);
		crumb = new BreadCrumbs(20, 100);
		//avoid = new AvoidPositions(BOUND, 2 * BOUND, .5f);
		
		boid = new Boid(200, 200,
				crumb,
				target,
				//avoid,
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1)
				);
		
		engine.addEntity(boid);	
		engine.begin();
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		highlight = quantize(e.getX(), e.getY());
		if (highlight == null) return;
		Grid player = quantize(boid.position().x, boid.position().y);
		if (player == null) return;
		if (Math.abs(highlight.x - player.x) <= 1 && Math.abs(highlight.y - player.y) <= 1)
			highlight = null;
		else if (nodes[highlight.y][highlight.x] == goal)
			highlight = null;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		highlight = null;
		if (rightDown) {
			Grid grid = quantize(e.getX(), e.getY());
			if (grid == null) return;

			goal = nodes[grid.y][grid.x];
			fineGoal = new PVector(e.getX(), e.getY());

			if (goal != null && !goal.valid) {
				goal = null;
				fineGoal = null;
			}
			if (goal != null) solve();
			else alg = null;
		} else
			adjust(e.getX(), e.getY());
	}

	private void initGrid() {
		nodes = new Node[(engine.height - BOUND * 2) / BOUND][(engine.width - BOUND * 2) / BOUND];
		xOff = BOUND + BOUND / 2 + ((engine.width - BOUND * 2) - (nodes[0].length * BOUND)) / 2;
		yOff = BOUND + BOUND / 2 + ((engine.height - BOUND * 2) - (nodes.length * BOUND)) / 2;
		int c = 0;
		for (int y = 0; y < nodes.length; y++) {
			for (int x = 0; x < nodes[0].length; x++) {
				nodes[y][x] = new Node(c, new PVector(xOff + x * BOUND, yOff + y * BOUND));
				c++;
			}
		}
		xOff -= BOUND / 2;
		yOff -= BOUND / 2;

		setEdges();
	}


	private void setEdges() {
		for (int y = 0; y < nodes.length; y++)
			for (int x = 0; x < nodes[0].length; x++)
				nodes[y][x].clearEdges();


		for (int y = 0; y < nodes.length; y++) {
			for (int x = 0; x < nodes[0].length; x++) {
				Node n = nodes[y][x];
				if (!n.valid) continue;
				if (x > 0 && nodes[y][x - 1].valid)
					n.doubleEdge(nodes[y][x - 1], PVector.dist(n.p, nodes[y][x - 1].p));
				if (y > 0 && nodes[y - 1][x].valid)
					n.doubleEdge(nodes[y - 1][x], PVector.dist(n.p, nodes[y - 1][x].p));

				if (x > 0 && y > 0 && nodes[y - 1][x - 1].valid && nodes[y][x - 1].valid && nodes[y - 1][x].valid)
					n.doubleEdge(nodes[y - 1][x - 1], PVector.dist(n.p, nodes[y - 1][x - 1].p));
				if (x > 0 && y < nodes.length - 1 && nodes[y + 1][x - 1].valid
						&& nodes[y + 1][x].valid && nodes[y][x - 1].valid)
					n.doubleEdge(nodes[y + 1][x - 1], PVector.dist(n.p, nodes[y + 1][x - 1].p));
			}
		}
	}


	private void solve() {
		if (goal == null) return;
		Grid grid = quantize(boid.position().x, boid.position().y);
		Node boidNode = nodes[grid.y][grid.x];
		if (goal == boidNode) {
			target.target(fineGoal.x, fineGoal.y);
			alg = null;
			return;
		}

		alg = new Algorithm(nodes[grid.y][grid.x], goal, GraphSolver.HEURISTICS[heuristic]);
		alg.step();
	}


	private void adjust(int x, int y) {
		if (rightDown) return;
		Grid grid = quantize(x, y);
		if (grid == null) return;
		Grid player = quantize(boid.position().x, boid.position().y);
		if (player == null) return;
		if (!shifting && Math.abs(grid.x - player.x) <= 1 && Math.abs(grid.y - player.y) <= 1) return;
		if (nodes[grid.y][grid.x] == goal) return;
		if (nodes[grid.y][grid.x].valid == shifting) return;
		nodes[grid.y][grid.x].valid = shifting;
		setEdges();
		solve();
	}


	private Grid quantize(float x, float y) {
		if (x < BOUND || x >= engine.width - BOUND || y < BOUND || y >= engine.height - BOUND)
			return null;
		int a = ((int) x - xOff) / BOUND;
		int b = ((int) y - yOff) / BOUND;
		if (a < 0) return null;
		if (b < 0) return null;
		if (a >= nodes[0].length) return null;
		if (b >= nodes.length) return null;
		return new Grid(a, b);
	}
	
	@Override
	public void draw(PGraphics g) {

		g.fill(0xffb200ff);
		for (Node[] y : nodes)
			for (Node x : y)
				if (!x.valid)
					g.rect(x.p.x - HALF, x.p.y - HALF, BOUND, BOUND);

		Grid grid = quantize(boid.position().x, boid.position().y);
		Node b = nodes[grid.y][grid.x];
		
//		avoid.clear();
//		for (Node n : group(grid.x, grid.y))
//			avoid.addPoint(n.p);
		
		if (smoothing) target.target(boid.position().x, boid.position().y);
		if (alg != null && alg.getSolved()) {
			g.stroke(0xffffff00);
			Node[] path = alg.getPath();

			Node n, m;
			
			for (int i = 0; i < path.length - 1; i++) {
				n = path[i];
				m = path[i + 1];
				g.line(n.p.x, n.p.y, m.p.x, m.p.y);

				//Skip pathfollowing if smoothing enabled
				if (smoothing) continue;
				
				if (n == b) {
					target.target(m.p.x, m.p.y);
					if (m == goal)
						target.target(fineGoal.x, fineGoal.y);
				}
			}
			
			if (smoothing) {			
				if (rayCast(g, fineGoal)) target.target(fineGoal.x, fineGoal.y);
				else {
					for (int i = path.length - 1; i >= 0; i--) {
						if (rayCast(g, path[i].p)) {
							target.target(path[i].p.x, path[i].p.y);
							break;
						}
					}
				}

			}
		}
			
		g.noStroke();
		if (fineGoal != null) {
			g.fill(0xffff9933);
			g.ellipse(fineGoal.x, fineGoal.y, NODE_SIZE, NODE_SIZE);
		}

		if (highlight != null) {
			g.fill(0x22ff00ff);
			g.rect(highlight.x * BOUND + BOUND, highlight.y * BOUND + BOUND, BOUND, BOUND);
		}

		g.fill(0xff000066);

		g.rect(0, 0, BOUND, engine.height);
		g.rect(0, 0, engine.width, BOUND);
		g.rect(engine.width, 0, -BOUND, engine.height);
		g.rect(0, engine.height, engine.width, -BOUND);
	}

	
	public static String toFileName(String name) {
		if (name == null) return null;
		return "data/" + name.split("\\.")[0] + ".map";
	}
	
	
	private float localize(int p) {
		return BOUND + HALF + p * BOUND;
	}
	
	private void openfile(String file) {
		alg = null;
		goal = null;
		fineGoal = null;
		try {
			Scanner in = new Scanner(new File(file));
			crumb.clear();
			for (Node[] y : nodes)
				for (Node x : y)
					x.valid = true;
			boid.setPosition(localize(in.nextInt()), localize(in.nextInt()));
			target.target(boid.position().x, boid.position().y);
			while (in.hasNextInt()) {
				int x = in.nextInt();
				int y = in.nextInt();
				nodes[y][x].valid = false;
			}
			in.close();
		} catch (Exception e) {}
		setEdges();
	}
	
	
	private void write(String file) {
		try {
			PrintStream writer = new PrintStream(new FileOutputStream(file));
			Grid grid = quantize(boid.position().x, boid.position().y);
			writer.println(grid.x + " " + grid.y);
			for (int y = 0; y < nodes.length; y++)
				for (int x = 0; x < nodes[0].length; x++)
					if (!nodes[y][x].valid)
						writer.println(x + " " + y);
			writer.close();
		} catch (Exception e) {
		}
	}
	

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getButton() == PApplet.RIGHT) rightDown = false;
	}


	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == PApplet.LEFT) {
			adjust(e.getX(), e.getY());
			return;
		}

		if (e.getButton() != PApplet.RIGHT) return;
		rightDown = true;
		highlight = null;

		Grid grid = quantize(e.getX(), e.getY());
		if (grid == null) return;

		goal = nodes[grid.y][grid.x];
		fineGoal = new PVector(e.getX(), e.getY());

		if (goal != null && !goal.valid) {
			goal = null;
			fineGoal = null;
		}
		if (goal != null) solve();
		else alg = null;
	}


	private boolean shifting = false;

	
	@Override
	public void keyReleased(KeyEvent e) {
		shifting = e.isShiftDown();
	}

	
	@Override
	public void keyPressed(KeyEvent e) {
		shifting = e.isShiftDown();
		switch (e.getKey()) {
		case 'r':
			crumb.clear();
			for (Node[] y : nodes)
				for (Node x : y)
					x.valid = true;
			setEdges();
			solve();
			break;
		case 's':
			smoothing = !smoothing;
			if (!smoothing)
				solve();
			break;
		case 'p':
			write(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		case 'o':
			openfile(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		default:
			int i = e.getKey() - '1';
			if (i >= 0 && i < GraphSolver.HEURISTICS.length) {
				heuristic = i;
				if (alg != null)
					solve();
			}
		}
	}


	private class Grid {
		int x; int y;
		public Grid(int x, int y) {this.x = x; this.y = y;}
	}


	@Override
	public String getName() {
		return "Navigation | " + GraphSolver.HEURISTICS[heuristic].getName() +
				" | " + (smoothing ? "Path Smoothing" : "Path Following");
	}


	private final float RECT_POINTS[][] = {
			{-HALF, -HALF, -HALF, HALF},
			{-HALF, -HALF, HALF, -HALF},
			{-HALF, HALF, HALF, HALF},
			{HALF, -HALF, HALF, HALF},
	};

	
	private boolean rayCast(PGraphics gg, PVector dest) {
		HashSet<Node> check = getRegion(dest);
		if (check == null) return false;
		
		gg.smooth();
		gg.noStroke();
		gg.fill(0x33ff0000);
		
		for (Node n : check) {
			for (float[] i : RECT_POINTS) {
				PVector intersection = Rays.segmentIntersectionPoint(
						boid.position().x, boid.position().y, dest.x, dest.y,
						n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);
				
				if (intersection == null) {
					float dx = boid.position().x - dest.x;
					float dy = boid.position().y - dest.y;
					PVector normal = new PVector(-dy, dx);
					normal.normalize();
					normal.mult(Boid.DEFAULT_SIZE);
					
					//Normals to avoid corner cutting
					PVector intersection1 = Rays.segmentIntersectionPoint(
							boid.position().x + normal.x, boid.position().y + normal.y, dest.x + normal.x, dest.y + normal.y,
							n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);
					PVector intersection2 = Rays.segmentIntersectionPoint(
							boid.position().x - normal.x, boid.position().y - normal.y, dest.x - normal.x, dest.y - normal.y,
							n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);
				
					if (intersection1 == null && intersection2 == null) continue;		
				}
				
				gg.rect(n.p.x - HALF, n.p.y - HALF, BOUND, BOUND);
				return false;
			}
		}
		
		gg.stroke(0xaaff0000);
		gg.line(boid.position().x, boid.position().y, dest.x, dest.y);
		return true;
	}


	
	
	
	private HashSet<Node> getRegion(PVector dest) {
		Grid b = quantize(boid.position().x, boid.position().y);
		Grid g = quantize(dest.x, dest.y);
		if (b == null | g == null) return null;

		HashSet<Node> check = new HashSet<>();

		int deltax = b.x - g.x;
		int deltay = b.y - g.y;
		float error = 0;

		if (deltax == 0) {
			for (int y = Math.min(b.y, g.y); y <= Math.max(b.y, g.y); y++) {
				group(check, b.x, y);
			}
		} else {
			float deltaerr = Math.abs((float) deltay / deltax);
			int y = b.y;
			int x;
			for (x = b.x; b.x < g.x ? x <= g.x : x >= g.x; x += Math.signum(g.x - b.x)) {
				group(check, x, y);
				error += deltaerr;
				while (error >= 0.5f) {
					group(check, x, y);
					y += Math.signum(g.y - b.y);
					error -= 1;
				}
			}
		}
		return check;
	}


	@SuppressWarnings("unused")
	private HashSet<Node> group(int xx, int yy) {
		HashSet<Node> set = new HashSet<>();
		group(set, xx, yy);
		return set;
	}
	
	
	private void group(HashSet<Node> check, int xx, int yy) {
		for (int x = xx - 1; x <= xx + 1; x++)
			for (int y = yy - 1; y <= yy + 1; y++)
				if (x >= 0 && x < nodes[0].length && y >= 0 && y < nodes.length && !nodes[y][x].valid)
					check.add(nodes[y][x]);
	}

	public static void main(String[] args) {new Game();}
}
