package runnable;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;

@SuppressWarnings("serial")
public class Rays extends PApplet {

	public static final float SCALE = 0.5f;
	public static final float ERR = 0.001f;
	
	int n = 0;
	float[] p = new float[8];
	PVector point;
	int state;
	
	@Override
	public void setup() {
		size((int) (displayWidth * SCALE), (int) (displayHeight * SCALE));
		fill(0xffff0000);
		strokeWeight(2);
		smooth();
		ellipseMode(CENTER);
		frame.setTitle("Ray Casting Test");
	}
	
	
	@Override
	public void draw() {
		background(0xff000000);
		stroke(0xff0000ff);
		if (n < 4) {
			if (n < 2) return;
			line(p[0], p[1], mouseX, mouseY);
			return;
		}
		line(p[0], p[1], p[2], p[3]);
		stroke(0xffff0000);
		if (n < 8) {
			if (n < 6) return;
			line(p[4], p[5], mouseX, mouseY);
			point = intersection(p[0], p[1], p[2], p[3], p[4], p[5], mouseX, mouseY);
			if (point != null) {
				fill(0xffffff00);
				noStroke();
				ellipse(point.x, point.y, 5,5);
			}
			return;
		}
		line(p[4], p[5], p[6], p[7]);
		if (point == null) return;
		noStroke();
		
		switch(state) {
		case 0: fill(0xffff00ff); break;
		case 1: fill(0xff0000ff); break;
		case 2: fill(0xffff0000); break;
		case 3: fill(0xffffffff); break;
		}
		
		ellipse(point.x, point.y, 5,5);
	}
	
	
	public static PVector intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
		if (d == 0) return null;
		float a = x1 * y2 - y1 * x2;
		float b = x3 * y4 - y3 * x4;
		return new PVector((a * (x3 - x4) - b * (x1 - x2)) / d, (a * (y3 - y4) - b * (y1 - y2)) / d);
	}
	
	
	public static boolean onLine(float x1, float y1, float x2, float y2, PVector p) {
		return p.x >= Math.min(x1, x2) - ERR && p.x <= Math.max(x1, x2) + ERR
				&& p.y >= Math.min(y1, y2) - ERR && p.y <= Math.max(y1, y2) + ERR;
	}
	
	
	public static boolean segmentIntersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		PVector point = intersection(x1, y1, x2, y2, x3, y3, x4, y4);
		return point != null && onLine(x1, y1, x2, y2, point) &&  onLine(x3, y3, x4, y4, point);
	}
	
	
	public static PVector segmentIntersectionPoint(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		PVector point = intersection(x1, y1, x2, y2, x3, y3, x4, y4);
		if (point != null && onLine(x1, y1, x2, y2, point) &&  onLine(x3, y3, x4, y4, point))
			return point;
		return null;
	}
	
	
	@Override
	public void mousePressed(MouseEvent event) {
		if (n == 8) n = 0;
		p[n] = event.getX();
		n++;
		p[n] = event.getY();
		n++;
		if (n == 8) {
			point = intersection(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);
			if (point == null) return;
			if (onLine(p[0], p[1], p[2], p[3], point)) {
				if (onLine(p[4], p[5], p[6], p[7], point))
					state = 0;
				else 
					state = 1;
			} else if (onLine(p[4], p[5], p[6], p[7], point)) {
				state = 2;
			} else {
				state = 3;
			}
		}
	}
	
	
	public static void main(String[] args) {
		PApplet.main("runnable.Rays");
	}
	
}
