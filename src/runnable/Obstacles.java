package runnable;

import graphs.Algorithm;
import graphs.Node;

import java.util.HashSet;
import java.util.Random;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;
import util.Triangle;

@SuppressWarnings("serial")
public class Obstacles extends PApplet {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;
	
	private static final int BOUND = 20;
	
	private static final int BRIGHT = 180;
	private static final int SUB_BRIGHT = 255 - BRIGHT;
	
	private static final int NODE_SPACING = 24;
	private static final int NODE_SIZE = 8;
	
	private static final int OBSTACLES = 70;
	private static final int O_SIDE_MAX = 150;
	private static final int O_SIDE_MIN = 80;
	private static final int O_AREA = 700;
	private static final int O_SEP = 40;
	
	private boolean drawPaths = true;
	
	private Random r = new Random();
	private Triangle[] o = new Triangle[OBSTACLES];
	
	private Node[][] nodeGrid;
	private Node start, end;
	private Algorithm alg;
	private int xOff, yOff;
	private int heuristic = 1;
	
	
	@Override
	public void setup() {
		size(WIDTH, HEIGHT);
		ellipseMode(CENTER);
		smooth();
		initGrid();
		randomize();
		title();
	}
	
	
	private void initGrid() {
		nodeGrid = new Node[(height - BOUND * 2) / NODE_SPACING + 1][(width - BOUND * 2) / NODE_SPACING + 1];
		xOff = BOUND + NODE_SPACING / 2 + ((width - BOUND * 2) - (nodeGrid[0].length * NODE_SPACING)) / 2;
		yOff = BOUND + NODE_SPACING / 2 + ((height - BOUND * 2) - (nodeGrid.length * NODE_SPACING)) / 2;
		int c = 0;
		for (int y = 0; y < nodeGrid.length; y++) {
			for (int x = 0; x < nodeGrid[0].length; x++) {
				nodeGrid[y][x] = new Node(c, new PVector(xOff + x * NODE_SPACING, yOff + y * NODE_SPACING));
				c++;
			}
		}
		xOff -= NODE_SPACING / 2;
		yOff -= NODE_SPACING / 2;
	}
	
	
	private boolean separated(int i, Triangle t) {
		for (; i >= 0; i--)
			if (t.minDist(o[i]) < O_SEP)
				return false;
		return true;
	}
	
	
	private void solve() {
		alg = new Algorithm(start, end, GraphSolver.HEURISTICS[heuristic]);
		alg.step();
		title();
	}
	
	
	private int brightColor() {
		return 0xff000000 | 
				(BRIGHT + r.nextInt(SUB_BRIGHT)) << 16 |
				(BRIGHT + r.nextInt(SUB_BRIGHT)) << 8 |
				(BRIGHT + r.nextInt(SUB_BRIGHT));
	}
	

	private Grid quantize(float x, float y) {
		int a = ((int) x - xOff) / NODE_SPACING;
		int b = ((int) y - yOff) / NODE_SPACING;
		if (a < 0) a = 0;
		if (b < 0) b = 0;
		if (a >= nodeGrid[0].length) a = nodeGrid[0].length - 1;
		if (b >= nodeGrid.length) b = nodeGrid.length - 1;
		return new Grid(a, b);
	}
	
	
//	private PVector localize() {
//		return null;
//	}
	
	
	private void collision(Triangle t) {
		Grid min = quantize(t.minX(), t.minY());
		Grid max = quantize(t.maxX(), t.maxY());
		for (int y = min.y; y <= max.y; y++) {
			for (int x = min.x; x <= max.x; x++) {
				Node n = nodeGrid[y][x];
				if (!n.valid) continue;
				if (t.contains(n.p.x, n.p.y))
					nodeGrid[y][x].valid = false;
				else
					n.obstacles.add(t);
			}
		}
	}
	
	
	private boolean verifyEdge(Node a, Node b) {
		HashSet<Triangle> checked = new HashSet<>();
		for (Triangle t : a.obstacles) {
			if (t.intersects(a.p.x, a.p.y, b.p.x, b.p.y))
				return false;
			checked.add(t);
		}
		for (Triangle t : b.obstacles) {
			if (!checked.contains(t) && t.intersects(a.p.x, a.p.y, b.p.x, b.p.y))
				return false;
		}
		return true;
	}
	
	
	private void randomize() {
		int x, y, bright;
		
		int oSide = r.nextInt(O_SIDE_MAX - O_SIDE_MIN) + O_SIDE_MIN;
		int half = oSide / 2;
		
		for (y = 0; y < nodeGrid.length; y++) {
			for (x = 0; x < nodeGrid[0].length; x++) {
				nodeGrid[y][x].valid = true;
				nodeGrid[y][x].clearEdges();
				nodeGrid[y][x].obstacles.clear();
			}
		}
		
		for (int i = 0; i < o.length; i++) {
			bright = brightColor();
			do {
				x = r.nextInt(width);
				y = r.nextInt(height);
				do {
					o[i] = new Triangle(x, y, x + r.nextInt(oSide) - half, y + r.nextInt(oSide) - half,
							x + r.nextInt(oSide) - half, y + r.nextInt(oSide) - half, bright);
				} while (o[i].area() < O_AREA);
			} while (!separated(i - 1, o[i]));
			collision(o[i]);
		}
		
		for (y = 0; y < nodeGrid.length; y++) {
			for (x = 0; x < nodeGrid[0].length; x++) {
				Node n = nodeGrid[y][x];
				if (!n.valid) continue;
				if (x > 0 && nodeGrid[y][x - 1].valid && verifyEdge(n, nodeGrid[y][x - 1]))
					n.doubleEdge(nodeGrid[y][x - 1], PVector.dist(n.p, nodeGrid[y][x - 1].p));
				if (y > 0 && nodeGrid[y - 1][x].valid && verifyEdge(n, nodeGrid[y - 1][x]))
					n.doubleEdge(nodeGrid[y - 1][x], PVector.dist(n.p, nodeGrid[y - 1][x].p));
				if (x > 0 && y > 0 && nodeGrid[y - 1][x - 1].valid && verifyEdge(n, nodeGrid[y - 1][x - 1]))
					n.doubleEdge(nodeGrid[y - 1][x - 1], PVector.dist(n.p, nodeGrid[y - 1][x - 1].p));
				if (x > 0 && y < nodeGrid.length - 1 && nodeGrid[y + 1][x - 1].valid && verifyEdge(n, nodeGrid[y + 1][x - 1]))
					n.doubleEdge(nodeGrid[y + 1][x - 1], PVector.dist(n.p, nodeGrid[y + 1][x - 1].p));
			}
		}

		if (start != null && end != null) {
			if (start.valid && end.valid) {
				solve();
				if (!alg.getSolved())
					randomize();
			} else {
				randomize();
			}
		} else {
			alg = null;
		}
	}
	
	
	@Override
	public void draw() {
		background(0xff000000);
		noStroke();
		
		for (Triangle t : o)
			t.draw(g);
		
		if (drawPaths) {			
			stroke(0xff777777);
			for (int y = 0; y < nodeGrid.length; y++)
				for (int x = 0; x < nodeGrid[0].length; x++)
					if (nodeGrid[y][x].valid)
						for (Node n : nodeGrid[y][x].edges)
							line(nodeGrid[y][x].p.x, nodeGrid[y][x].p.y, n.p.x, n.p.y);			
			if (alg != null && alg.getSolved()) {
				stroke(0xffffff00);
				Node[] path = alg.getPath();
				Node n, m;
				for (int i = 0; i < path.length - 1; i++) {
					n = path[i];
					m = path[i + 1];
					line(n.p.x, n.p.y, m.p.x, m.p.y);
				}
			}
			noStroke();
			
			/*
			fill(0xffff00ff);
			for (int y = 0; y < nodeGrid.length; y++)
				for (int x = 0; x < nodeGrid[0].length; x++)
					if (nodeGrid[y][x].valid && nodeGrid[y][x].edges.size() > 0)
						ellipse(nodeGrid[y][x].p.x, nodeGrid[y][x].p.y, NODE_SIZE, NODE_SIZE);
			*/
			
			if (start != null) {
				fill(0xff00ff00);
				ellipse(start.p.x, start.p.y, NODE_SIZE, NODE_SIZE);
			}
			if (end != null) {
				fill(0xffff9933);
				ellipse(end.p.x, end.p.y, NODE_SIZE, NODE_SIZE);
			}
		}
		
		fill(0xff000066);
		rect(0, 0, BOUND, height); rect(0, 0, width, BOUND);
		rect(width, 0, -BOUND, height); rect(0, height, width, -BOUND);
	}
	
	
	@Override
	public void mousePressed(MouseEvent event) {
		Grid grid = quantize(mouseX, mouseY);
		if (mouseButton == LEFT)
			start = nodeGrid[grid.y][grid.x];
		if (mouseButton == RIGHT)
			end = nodeGrid[grid.y][grid.x];
		if (start == end) end = null;
		if (start != null && !start.valid) start = null;
		if (end != null && !end.valid) end = null;
		if (start != null && end != null) solve();
		else alg = null;
	}
	
	
	@Override
	public void keyPressed() {
		switch (key) {
		case 'r': randomize(); break;
		default:
			int i = key - '1';
			if (i >= 0 && i < GraphSolver.HEURISTICS.length) {
				heuristic = i;
				title();
				if (alg != null)
					solve();
			}
		}
	}
	
	
	private void title() {
		String title = "'R' to randomize | L/R click to set start/end | Using heuristic: " +
				GraphSolver.HEURISTICS[heuristic].getName();
		if (alg != null && alg.getSolved())
			title += String.format(" | Length: %.1f", alg.getPathLength());
		frame.setTitle(title);
	}
	
	
	private class Grid {
		int x; int y;
		public Grid(int x, int y) {this.x = x; this.y = y;}
	}
	
	
	public static void main(String[] args) {
		PApplet.main("runnable.Obstacles");
	}
}
