package runnable;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import processing.core.PApplet;
import processing.core.PVector;


public class GenerateGraph {
	
	static final int NODES = 5000;
	static final int DENSITY = 15;
	static final int CORE = 2;
	static final int EXTRA = 300;
	static final int RAD = 10;
	static final int EFFORT = 1000;
	
	static final String FILE = "testeee";
	
	static boolean READIN = true;
	
	
	public static void main(String[] args) throws Exception {
		int n, side, c, m, rad, be;
		float p;
		String file;
		if (READIN) {
			Scanner in = new Scanner(System.in);
			
			System.out.printf("Nodes: ");
			n = in.nextInt();
			
			System.out.printf("Density %%: ");
			p = in.nextInt() / 100f;
			side = (int) Math.sqrt(n / p);
			System.out.println("Max size of " + side);
			
			System.out.printf("Core Edges Per Node: ");
			c = in.nextInt();
			
			System.out.printf("Extra Edges: ");
			m = in.nextInt();
			
			System.out.println("Total edges: " + (m + c * n));
			
			System.out.printf("Radius: ");
			rad = in.nextInt();
			
			System.out.printf("BestEffort: ");
			be = in.nextInt();
			
			System.out.printf("File: ");
			file = in.next();
			in.close();
		} else {
			n = NODES;
			side = (int) Math.sqrt(n / (DENSITY / 100f));
			System.out.println("Max size of " + side);
			c = CORE;
			m = EXTRA;
			rad = RAD;
			be = EFFORT;
			file = FILE;
		}
		file = GraphDraw.toFileName(file);
		PrintStream writer = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)));
		writer.println(n);
		
		int fail = 0;
		
		Set<String> nodes = new HashSet<>();
		PVector[] no = new PVector[n]; 
		
		Random r = new Random();
		
		for (int i = 0; i < n; i++) {
			int x, y;
			String build;
			do {
				x = r.nextInt(side);
				y = r.nextInt(side);
				build = x + " " + y; 
			} while (nodes.contains(build));
			nodes.add(build);
			no[i] = new PVector(x, y);
			writer.println(build);
		}
		
		writer.println(m + n * c);
		Set<String> edges = new HashSet<>(m + n);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < c; j++) {
			int pick = 0;
				String build = null;
				do {
					for (int k = 0; k < be; k++) {
						pick = r.nextInt(n);
						if (PVector.dist(no[i], no[pick]) <= rad)
							break;
					}
					build = Math.min(i, pick) + " " + Math.max(i, pick);
				} while (pick == i || edges.contains(build));
				if (PVector.dist(no[i], no[pick]) > rad) fail++;
				edges.add(build);
				writer.println(build);
			}
		}
		
		for (int i = 0; i < m; i++) {
			int x = -1, y = -1;
			String build;
			do {
				for (int k = 0; k < be; k++) {
					x = r.nextInt(n);
					y = r.nextInt(n);
					if (PVector.dist(no[x], no[y]) <= rad)
						break;
				}
				build = Math.min(x, y) + " " + Math.max(x, y);
			} while (x == y || edges.contains(build));
			if (PVector.dist(no[x], no[y]) > rad) fail++;
			edges.add(build);
			writer.println(build);
		}

		writer.close();
		System.out.println("Failure rate: " + (100 * fail / (float) (m + c * n)) + "%");
		GraphSolver.opener = file;
		PApplet.main("runnable.GraphSolver");
		
	}
}
