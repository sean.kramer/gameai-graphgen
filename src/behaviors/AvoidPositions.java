package behaviors;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class AvoidPositions extends BlendableBehavior {

	private float radius;
	private float buffer;
	ArrayList<PVector> avoid;
	
	
	public AvoidPositions(float buffer, float radius, float strength) {
		super(strength);
		avoid = new ArrayList<PVector>();
		this.radius = radius;
		this.buffer = radius - buffer;
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {

		g.stroke(0xff800000);
		for (PVector e : avoid) {
			float dist = PVector.dist(boid.position(), e);
			if (dist > radius) continue;
			
			PVector acceleration = PVector.sub(boid.position(), e);
			acceleration.normalize();
			
			float calc = (radius - dist) / buffer;
			acceleration.mult(calc * strength);
			boid.accelerate(acceleration);

			//if (draw()) {
				g.stroke(200, 0, 0, calc * 255);
				g.strokeWeight(2);
				g.line(boid.position().x, boid.position().y, e.x, e.y);
			//}
		}
	}


	public void clear() {
		avoid.clear();
	}
	
	
	public void addPoint(PVector entity) {
		avoid.add(entity);
	}

}
