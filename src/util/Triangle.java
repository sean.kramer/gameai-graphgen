package util;

import processing.core.PApplet;
import processing.core.PGraphics;
import runnable.Rays;

public class Triangle {

	private float ax, ay, bx, by, cx, cy;
	private int color;
	
	public Triangle(float ax, float ay, float bx, float by, float cx, float cy, int color) {
		this.ax = ax; this.ay = ay;
		this.bx = bx; this.by = by;
		this.cx = cx; this.cy = cy;
		this.color = color;
	}
	
	private float sign (float x1, float y1, float x2, float y2, float x3, float y3) {
	    return (x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3);
	}

	public boolean contains(float px, float py) {
	    boolean b1 = sign(px, py, ax, ay, bx, by) < 0;
	    boolean b2 = sign(px, py, bx, by, cx, cy) < 0;
	    boolean b3 = sign(px, py, cx, cy, ax, ay) < 0;
	    return ((b1 == b2) && (b2 == b3));
	}
	
	
	public float area() {
		return PApplet.abs((ax - cx) * (by - ay) - (ax - bx) * (cy - ay)) / 2;
	}
	
	
	public float minDist(Triangle t) {
		float[] x = new float[] {
				ax - t.ax, ax - t.bx, ax - t.cx,
				bx - t.ax, bx - t.bx, bx - t.cx,
				cx - t.ax, cx - t.bx, cx - t.cx,
		};
		float[] y = new float[] {
				ay - t.ay, ay - t.by, ay - t.cy,
				by - t.ay, by - t.by, by - t.cy,
				cy - t.ay, cy - t.by, cy - t.cy,
		};
		float min = Float.MAX_VALUE;
		for (int i = 0; i < x.length; i++) {
			float val = x[i] * x[i] + y[i] * y[i];
			if (val < min) min = val;
		}
		return PApplet.sqrt(min);
	}
	
	
	public boolean intersects(float x1, float y1, float x2, float y2) {
		return Rays.segmentIntersection(x1, y1, x2, y2, ax, ay, bx, by) ||
				Rays.segmentIntersection(x1, y1, x2, y2, ax, ay, cx, cy) ||
				Rays.segmentIntersection(x1, y1, x2, y2, bx, by, cx, cy);
	}
	
	
	public float minX() {return Math.min(Math.min(ax, bx), cx);}
	public float minY() {return Math.min(Math.min(ay, by), cy);}
	public float maxX() {return Math.max(Math.max(ax, bx), cx);}
	public float maxY() {return Math.max(Math.max(ay, by), cy);}
	
	
	public void draw(PGraphics g) {
		g.fill(color);
		g.triangle(ax, ay, bx, by, cx, cy);
	}
}
