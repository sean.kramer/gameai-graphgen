package util;
import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public abstract class Simulation {

	protected Engine engine;
	
	public Simulation() {
		engine = Engine.boot(this);
	}

	public abstract String getName();
	public abstract void draw(PGraphics g);
	
	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	public void mousePressed(MouseEvent e) {};
	public void mouseReleased(MouseEvent e) {};
	public void mouseDragged(MouseEvent e) {};
	public void mouseMoved(MouseEvent e) {};

}
