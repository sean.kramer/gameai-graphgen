package util;
import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import entities.Entity;


public class Engine extends PApplet {
	
	public static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;
	
	public static boolean drawBehaviors = false;
	
	private static Engine engine = null;
	private static Simulation sim;
	private boolean running = false;
			
	private ArrayList<Entity> entities;
	
	public static Engine boot(Simulation simulation) {
		sim = simulation;
		PApplet.main("util.Engine");
		while (engine == null)
			try {Thread.sleep(10);} catch (Exception e) {}
		return engine;
	}
	
	public void begin() {
		running = true;
	}
	
	public void keyPressed(KeyEvent e) {sim.keyPressed(e);}
	public void keyReleased(KeyEvent e) {sim.keyReleased(e);}
	
	public void mouseMoved(MouseEvent e) {sim.mouseMoved(e);}
	public void mouseDragged(MouseEvent e) {sim.mouseDragged(e);}
	public void mousePressed(MouseEvent e) {sim.mousePressed(e);}
	public void mouseReleased(MouseEvent e) {sim.mouseReleased(e);}
	
	public void setup() {
		engine = this;
		entities = new ArrayList<Entity>();

		size(WIDTH, HEIGHT);
		
		frame.setTitle(sim.getName());
		frameRate(60);
		frame.setResizable(false);
		
		ellipseMode(CENTER);
		smooth();
	}
	
	
	public void draw() {
		background(0xffcccccc);
		if (!running) return;
		
		sim.draw(g);
		frame.setTitle(sim.getName());
		
		for (Entity e : entities) {
			e.applyBehaviors(this);
			e.update();
		}
		
		for (Entity e : entities)
			e.draw(this);
	}

	
	public void addEntity(Entity e) {
		entities.add(e);
	}

	
	public List<Entity> entities() {
		return entities;
	}
}








