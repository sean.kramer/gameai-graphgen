package heuristics;

import graphs.Node;

public abstract class Heuristic {
	public abstract float calc(Node current, Node target);
	public abstract String getName();
}
