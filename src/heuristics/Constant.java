package heuristics;

import graphs.Node;

public class Constant extends Heuristic {
	int constant;
	public Constant(int constant) {this.constant = constant;}
	public float calc(Node current, Node end) {return constant;}
	public String getName() {return "Constant (Dijkstra)";}
}
