package heuristics;

import graphs.Node;

public class Manhattan extends Heuristic {
	public float calc(Node current, Node end) {return Math.abs(current.p.x - end.p.x) + Math.abs(current.p.y - end.p.y);}
	public String getName() {return "Manhattan Distance";}
}
