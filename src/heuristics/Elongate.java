package heuristics;

import processing.core.PVector;
import graphs.Node;

public class Elongate extends Heuristic {
	public float calc(Node current, Node end) {return -current.g * 2 - PVector.dist(current.p, end.p);}
	public String getName() {return "Elongated";}
}
