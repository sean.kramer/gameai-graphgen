package heuristics;

import graphs.Node;
import processing.core.PVector;

public class QuasiRandom extends Heuristic {
	public float calc(Node current, Node end) {return (float) Math.random() * PVector.dist(current.p, end.p);}
	public String getName() {return "Random Variance Linear";}
}
