package graphs;
import heuristics.Constant;
import heuristics.Heuristic;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Stack;

import processing.core.PVector;


public class Algorithm {
	
	public static final int SOLVED = 1;
	public static final int RUNNING = 0;
	public static final int NO_SOLUTION = -1;
	
	public int time = 0;
	
	private int state = RUNNING;
	private int step = 0;
	private float pathLenghth = 0;
	
	private Node goal;
	private Node[] path;
	private PriorityQueue<Node> open;
	private HashSet<Node> opened;
	private HashSet<Node> closed;
	private Heuristic heuristic;
	
	private static boolean instant = true;
	public static void toggleStepping() {
		instant = !instant;
	}

	
	public Algorithm(Node start, Node end) {
		this(start, end, new Constant(0));
	}
	
	
	public Algorithm(Node start, Node goal, Heuristic heuristic) {
		this.heuristic = heuristic;
		this.goal = goal;
		open = new PriorityQueue<>();
		opened = new HashSet<>();
		closed = new HashSet<>();
		start.g = 0;
		start.h = heuristic.calc(start, goal);
		start.f();
		start.parent = null;
		open.add(start);
	}

	
	/** Apply one step of the algorithm. */
	public void step() {
		if (state != RUNNING) return;
		long clock = System.nanoTime();
		do {
			step++;
			stepHelper();
		} while (state == RUNNING && instant);
		clock = System.nanoTime() - clock;
		time = (int) (clock / 1000000);
	}
	
	
	/** Apply the guts of the algorithm. */
	private void stepHelper() {	
		//Check end conditions
		if (closed.contains(goal)) {
			state = SOLVED;
			path();
			return;
		} else if (open.size() == 0) {
			state = NO_SOLUTION;
			return;
		}
		
		//Add best candidate to closed list
		Node candidate = open.poll();
		closed.add(candidate);
		
		//Set edges appropriately
		for (int i = 0; i < candidate.edges.size(); i++) {
			Node n = candidate.edges.get(i);
			
			//Re-check closed nodes
			if (closed.contains(n)) continue;
			
			//If node has already been visited, check to see if this path is shorter
			if (opened.contains(n)) {
				if (candidate.g + candidate.weights.get(i) < n.g) {
					open.remove(n);
					n.g = candidate.g + candidate.weights.get(i);
					n.f();
					n.parent = candidate;
					open.add(n);
				}
				
			//Otherwise, just add the node to the open list
			} else {
				n.g = candidate.g + candidate.weights.get(i);
				n.h = heuristic.calc(n, goal);
				n.f();
				n.parent = candidate;
				open.add(n);
				opened.add(n);
			}
		}
	}
	
	
	/** Trace back the path to get from start to finish. */
	private void path() {
		Stack<Node> stack = new Stack<>();
		Node n = goal;
		while (n != null) {
			stack.push(n);
			n = n.parent;
		}
		int size = stack.size();
		path = new Node[size];
		for (int i = 0; i < size; i++)
			path[i] = stack.pop();
		for (int i = 0; i < path.length - 1; i++)
			pathLenghth += PVector.dist(path[i].p, path[i + 1].p);
	}
	
	
	public int getStep() {return step;}
	public int getState() {return state;}
	public float getPathLength() {return pathLenghth;}
	public boolean getSolved() {return state == SOLVED;}
	public String getHeuristicName() {return heuristic.getName();}
	public Heuristic getHeuristic() {return heuristic;}
	public Iterable<Node> getClosed() {return closed;}
	public Iterable<Node> getOpen() {return open;}
	public Node[] getPath() {return path;}
	public static boolean getStepping() {return !instant;}
}
